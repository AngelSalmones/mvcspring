package com.softtek.academy.javaweb.springmvcapp.service;

import static org.junit.Assert.assertSame;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.softtek.academy.javaweb.springmvcapp.configuration.JDBCConfiguration;
import com.softtek.academy.javaweb.springmvcapp.model.Student;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JDBCConfiguration.class })
@WebAppConfiguration
public class StudentServiceImplTest {

	@Autowired
	private StudentService studentService;
	
	@Test
	public void getStudentsTest() {
		List<Student> students = studentService.getStudents();
		int actualname = students.get(0).getId();
		int expected = 1;
		assertSame(expected, actualname);
	}
}
