<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ page import= "java.util.*" %>
     <%@ page import="com.softtek.academy.javaweb.springmvcapp.model.*" %>
     <%@ page import="com.softtek.academy.javaweb.springmvcapp.controller.*" %>
     <%@taglib uri = "http://www.springframework.org/tags/form" prefix="form" %>
    <%@ page isELIgnored="false"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Students</h2>
<h4>Ultimo agregado</h4>
	<form:label path="name">nombre: ${name}</form:label><br>
	<form:label path="age">edad: ${age}</form:label><br>
	<form:label path="id">id: ${id}</form:label><br>
<h4>Lista de estudiantes</h4>
<table border="1">
<tr>
<c:forEach items="${list}" var="list">
 <td> <h4>Nombre: ${list.name} </h4></td>
 <td><h4>Edad: ${list.age} </h4>    </td>    
 <td> <h4>Id: ${list.id} </h4>      </td> 
 <td><a href ="/springmvcapp/editemap/${list.id}"> Update </a></td>
 <td><a href = "/springmvcapp/deletePerson/${list.id}"> Delete </a></td>
 </tr>             
</c:forEach>

</table>
</body>
</html>