package com.softtek.academy.javaweb.springmvcapp.service;

import java.util.List;

import com.softtek.academy.javaweb.springmvcapp.model.Student;

public interface StudentService {
	int saveStudent(String name, int age, int id);
	int deleteStudent (int id);
	List<Student> getStudents();
	Student getStudentById(int id);
	Student update (Student p);
}
