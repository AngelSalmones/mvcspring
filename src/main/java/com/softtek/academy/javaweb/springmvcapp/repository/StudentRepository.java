package com.softtek.academy.javaweb.springmvcapp.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.softtek.academy.javaweb.sprigmvcapp.dao.StudentDao;
import com.softtek.academy.javaweb.springmvcapp.model.Student;

@Repository("studentRepository")
public class StudentRepository implements StudentDao{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public int saveStudent(String name, int age, int id) {
		 Student student = new Student();
		 student.setAge(age);
		 student.setId(id);
		 student.setName(name);
	        entityManager.persist(student);
		return 0;
	}

	@Override
	public int deleteStudent(int id) {
		entityManager.remove(entityManager.find(Student.class, id));
		return 0;
	}

	@Override
	public List<Student> getStudents() {
		// TODO Auto-generated method stub
		return (List<Student>)entityManager
				.createQuery("Select s from Student s", Student.class).getResultList();
	}

	@Override
	public Student getStudentById(int id) {
		// TODO Auto-generated method stub
		return entityManager.find(Student.class, id);
	}

	@Override
	public Student update(Student p) {
		// TODO Auto-generated method stub
		Student actual = entityManager.find(Student.class, p.getId());
		actual.setAge(p.getAge());
		actual.setName(p.getName());
		 entityManager.merge(actual);
		 
		return p;
	}
}
