package com.softtek.academy.javaweb.springmvcapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.softtek.academy.javaweb.sprigmvcapp.dao.StudentDao;
import com.softtek.academy.javaweb.springmvcapp.model.Student;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {

	@Autowired
	@Qualifier("studentRepository")
	private StudentDao studentDao;
	
	@Override
	public int saveStudent(String name, int age, int id) {
		// TODO Auto-generated method stub
		return studentDao.saveStudent(name, age, id);
	}

	@Override
	public int deleteStudent(int id) {
		// TODO Auto-generated method stub
		return studentDao.deleteStudent(id);
	}

	@Override
	public List<Student> getStudents() {
		// TODO Auto-generated method stub
		return studentDao.getStudents();
	}

	@Override
	public Student getStudentById(int id) {
		// TODO Auto-generated method stub
		return studentDao.getStudentById(id);
	}

	@Override
	public Student update(Student p) {
		// TODO Auto-generated method stub
		return studentDao.update(p);
	}

}
