package com.softtek.academy.javaweb.springmvcapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.javaweb.sprigmvcapp.dao.StudentDao;
import com.softtek.academy.javaweb.springmvcapp.model.Student;
import com.softtek.academy.javaweb.springmvcapp.service.StudentService;

@Controller
public class PersonController {
	@Autowired
	StudentService studentService;
	@RequestMapping (value = "/student", method = RequestMethod.GET)
	public ModelAndView student() {
		return new ModelAndView ("student", "command", new Student());
	}
	 @RequestMapping(value = "/addPerson", method = RequestMethod.POST)
	    public String addPerson(@ModelAttribute("SpringWeb")Student student, ModelMap model) {
	        model.addAttribute("name", student.getName());
	        model.addAttribute("age", student.getAge());
	        model.addAttribute("id", student.getId());
	        List<Student> list = studentService.getStudents();  
	        model.addAttribute("list", list);
	        studentService.saveStudent(student.getName(),student.getAge(),student.getId());
	        return "newPerson";
	    }
	 @RequestMapping(value = "/deletePerson/{id}", method = RequestMethod.GET)
	 	public String deletePerson(@PathVariable int id) {
		 studentService.deleteStudent(id);
			return "redirect:/viewmap";
	 }
	 @RequestMapping("/viewmap")    
	    public String viewemp(Student student, ModelMap model){    
		    List<Student> list = studentService.getStudents();  
	        model.addAttribute("list", list);
	        return "viewPerson";    
	    }   
	 @RequestMapping(value="/editemap/{id}")    
	    public String edit(@PathVariable int id, ModelMap m){    
		    Student stdao = studentService.getStudentById(id);    
	        m.addAttribute("command",stdao);  
	        return "editPerson";    
	    }  
	 @RequestMapping(value="/editsave",method = RequestMethod.POST)    
	    public String editsave(@ModelAttribute("emp") Student emp){    
		 studentService.update(emp);    
	        return "redirect:/viewmap";    
	    }    
}
